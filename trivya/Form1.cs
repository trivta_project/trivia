﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace trivya
{
    public partial class Form1 : Form
    {

        private TcpClient client = new TcpClient();
        private IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
        private NetworkStream clientStream;

        public Form1()
        {

            client.Connect(serverEndPoint);
            clientStream = client.GetStream();

            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Sign_In(object sender, EventArgs e)
        {
            //login
            if (Validator.isUsernameValid(username.Text) && Validator.isPasswordValid(password.Text))
            {
                string msg = "200";

                if (username.Text.Length < 10) msg += "0" + username.Text.Length.ToString();
                else msg += username.Text.Length.ToString();
                msg += username.Text;

                if (password.Text.Length < 10) msg += "0" + password.Text.Length.ToString();
                else msg += password.Text.Length.ToString();
                msg += password.Text;

                byte[] newBuffer = new ASCIIEncoding().GetBytes(msg);
                clientStream.Write(newBuffer, 0, newBuffer.Length);
                clientStream.Flush();

                try
                {

                    byte[] bufferIn = new byte[4];
                    int bytesRead = clientStream.Read(bufferIn, 0, 4);
                    string input = new ASCIIEncoding().GetString(bufferIn);

                    switch (Int32.Parse(input[3].ToString()))
                    {
                        case 0:
                            //login successfull
                            break;
                        case 1:
                            error_msg.Text = "Wrong Details";
                            break;
                        case 2:
                            error_msg.Text = "User is already connected";
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception) { }

            }
        }

        private void sign_up_Click(object sender, EventArgs e)
        {
            
            signUpScreen s = new signUpScreen();
            s.ShowDialog();
        }
    }
}
