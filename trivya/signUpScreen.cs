﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace trivya
{
    public partial class signUpScreen : Form
    {
        public signUpScreen()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Validator.isUsernameValid(username.Text) && Validator.isPasswordValid(password.Text))
            {
                string msg = "203";

                if (username.Text.Length < 10) msg += "0" + username.Text.Length.ToString();
                else msg += username.Text.Length.ToString();
                msg += username.Text;

                if (password.Text.Length < 10) msg += "0" + password.Text.Length.ToString();
                else msg += password.Text.Length.ToString();
                msg += password.Text;

                if (email.Text.Length < 10) msg += "0" + email.Text.Length.ToString();
                else msg += email.Text.Length.ToString();
                msg += email.Text;

                TcpClient client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                NetworkStream clientStream;

            
                client.Connect(serverEndPoint);
                clientStream = client.GetStream();


                byte[] newBuffer = new ASCIIEncoding().GetBytes(msg);
                clientStream.Write(newBuffer, 0, newBuffer.Length);
                clientStream.Flush();

                try
                {

                    byte[] bufferIn = new byte[4];
                    int bytesRead = clientStream.Read(bufferIn, 0, 4);
                    string input = new ASCIIEncoding().GetString(bufferIn);

                    switch (Int32.Parse(input[3].ToString()))
                    {

                        case 0:
                            this.Close();
                            break;
                        case 2:
                            error_msg.Text = "Username already exist";
                            break;
                        case 4:
                            error_msg.Text = "other error happend";
                            break;
                        default:
                            break;
                    }

                }
                catch (Exception){}
            }

            if (!Validator.isPasswordValid(password.Text));
            {

                error_msg.Text = "password is illegal";
            }
            if (!Validator.isUsernameValid(username.Text));
            {

                error_msg.Text = "username is illegal";
            }

        }
    }
}
