﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivya
{
    class Game
    {

        private List<Question> _questions = new List<Question>();
        private List<User> _players = new List<User>();
        private int _questions_no;
        private int _currQuestionIndex;
        private DataBase _db;
        private Dictionary<string, int> _results = new Dictionary<string, int>();
        private int _currentTurnAnswers;

        public Game(List<User> players, int questionNo, DataBase db)
        {

            _db = db;
            int res = _db.insertNewGame();
            if (res != 0) _db.initQuestions(questionNo);

            for (int i = 0; i < players.Count();  i++)
            {

                _players.Insert(i, players[i]);
                _players[i].setGame(this);
            }

            foreach (User player in _players)
            {

                _results[player.getUsername()] = 0;
            }
        }

        ~Game()
        {

            _questions.Clear();
            _players.Clear();
        }

        public void sendFirstQuestion()
        {

            sendQuestionToAllUsers();
        }

        public void handleFinishGame()
        {

            _db.updateGameStatus(0);

            string msg = "121";
            msg += _players.Count().ToString();

            foreach (User player in _players)
            {
                msg += player.getUsername().Length.ToString();
                msg += player.getUsername();

                if (_results[player.getUsername()] < 10) msg += ("0" + _results[player.getUsername()].ToString());
                else msg += _results[player.getUsername()].ToString();
            }

            foreach (User player in _players)
            {
                try
                {
                    player.setGame(null);
                    player.send(msg);
                }
                catch (Exception){}
            }
        }

        public bool handleNextTurn()
        {

            if (_players.Count() == 0)
            {

                handleFinishGame();
                return false;
            }
            else
            {

                if (_currentTurnAnswers == _players.Count())
                {

                    if (_currQuestionIndex == _questions_no)
                    {

                        handleFinishGame();
                        return false;
                    }
                    else
                    {

                        _currQuestionIndex++;
                        sendQuestionToAllUsers();
                    }
                }
            }

            return true;
        }

        public bool handleAnswerFromUser(User user, int answerNo, int time)
        {

            _currentTurnAnswers++;
            if (answerNo == _questions[_currQuestionIndex].getCorrectAnswerIndex()) _results[user.getUsername()]++;

            if (answerNo != 5) _db.addAnswerToPlayer(getID(), user.getUsername(), _currQuestionIndex, _questions[_currQuestionIndex].getAnswers()[answerNo], answerNo == _questions[_currQuestionIndex].getCorrectAnswerIndex(), time);
            else _db.addAnswerToPlayer(getID(), user.getUsername(), _currQuestionIndex, "", answerNo == _questions[_currQuestionIndex].getCorrectAnswerIndex(), time);

            foreach (User player in _players)
            {

                player.send("120" + (_questions[_currQuestionIndex].getCorrectAnswerIndex() == answerNo).ToString());
            }

            return handleNextTurn();
        }

        public bool leaveGame(User currUser)
        {

            if (_players.IndexOf(_players.Where(p => p.getUsername() == currUser.getUsername()).FirstOrDefault()) != -1) _players.Remove(currUser);

            return handleNextTurn();
        }

        public int getID()
        {

            return 17;
        }

        private bool insertGameToDB()
        {

            return true;
        }

        private void initQuestionsFromDB()
        {

        }

        private void sendQuestionToAllUsers()
        {

            _currentTurnAnswers = 0;

            string msg = "118";
            msg += _questions[_questions_no].getQuestion().Length.ToString();
            msg += _questions[_questions_no].getQuestion();

            foreach (String answer in _questions[_questions_no].getAnswers())
            {
                msg += answer.Length.ToString();
                msg += answer;
            }

            foreach (User player in _players)
            {
                try
                {

                    player.send(msg);
                }
                catch (Exception){}
            }
        }
    }
}
