﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivya
{
    static class Validator
    {

        static public bool isPasswordValid(string password)
        {
            
            if (password.Length < 4) return false;
            if (password.Contains(' ')) return false;
            if (password.IndexOfAny("0123456789".ToCharArray()) == -1) return false;
            if (password.ToLower().ToUpper().Equals(password)) return false; //check if there is any lower case char
            if (password.ToUpper().ToLower().Equals(password)) return false; //check if there is any upper case char

            return true;
        }

        static public bool isUsernameValid(string username)
        {

            if (!((username[0] < 'z' && username[0] > 'a') || (username[0] < 'Z' && username[0] > 'A'))) return false;
            if (username.Contains(' ')) return false;
            if (username.Length < 1) return false;

            return true;
        }
    }
}
