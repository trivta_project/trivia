﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivya
{
    class Question
    {

        private string _question;
        private string[] _answers = new string[4];
        private int _correctAnswerIndex;
        private int _id;

        public Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
        {

            _question = question;

            //TODO: make the places save randomly
            _answers[0] = correctAnswer;
            _answers[1] = answer2;
            _answers[2] = answer3;
            _answers[3] = answer4;

            _correctAnswerIndex = id;
        }

        public string getQuestion() => _question;

        public string[] getAnswers() => _answers;

        public int getCorrectAnswerIndex() => _correctAnswerIndex;

        public int getId() => _id;
    }
}
