#include "Game.h"

Game::Game(const vector<User*>& players, int questionNo, DataBase& db)
{
	_db = db;

	int res = _db.insertNewGame();
	if (res != 0) _db.initQuestions(questionNo);

	for (int i = 0; i < players.size(); i++)
	{

		_players.push_back(players[i]);
		_players[i]->setGame(this);
	}

	for (int i = 0; i < _players.size(); i++)
	{

		vector<User*>::iterator s = _players.begin();
		++s;
		for (int j = 0; j < i; j++)
		{

			++s;
		}
		_results[(*s)->getUsername()] = 0;
	}
}

Game::~Game()
{

	_questions.clear();
	_players.clear();
}

void Game::sendFirstQuestion()
{

	sendQuestionToAllUsers();
}

void Game::handleFinishGame()
{

	_db.updateGameStatus(0);

	string msg = "121";
	msg += to_string(_players.size());

	for (int i = 0; i < _players.size(); i++)
	{
		msg += to_string(_players[i]->getUsername().length());
		msg += _players[i]->getUsername();

		if (_results[_players[i]->getUsername()] < 10) msg += ("0" + to_string(_results[_players[i]->getUsername()]));
		else msg += to_string(_results[_players[i]->getUsername()]);
	}

	for (int i = 0; i < _players.size(); i++)
	{
		try
		{
			vector<User*>::iterator s = _players.begin();
			++s;

			for (int j = 0; j < i; j++)
			{

				++s;
			}
			(*s)->setGame(NULL);
			(*s)->send(msg);
		}
		catch (...) {}
	}
}

bool Game::handleNextTurn()
{

	if (_players.size() == 0)
	{

		handleFinishGame();
		return false;
	}
	else
	{

		if (_currentTurnAnswers == _players.size())
		{

			if (_currQuestionIndex == _questions_no)
			{

				handleFinishGame();
				return false;
			}
			else
			{

				_currQuestionIndex++;
				sendQuestionToAllUsers();
			}
		}
	}

	return true;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{

	_currentTurnAnswers++;
	if (answerNo == _questions[_currQuestionIndex]->getCorrectAnswerIndex()) _results[user->getUsername()]++;

	//if (answerNo != 5) _db->addAnswerToPlayer(getID(), user->getUsername(), _currQuestionIndex, _questions[_currQuestionIndex]->getAnswers()[answerNo], answerNo == _questions[_currQuestionIndex]->getCorrectAnswerIndex(), time);
	//else _db->addAnswerToPlayer(getID(), user->getUsername(), _currQuestionIndex, "", answerNo == _questions[_currQuestionIndex]->getCorrectAnswerIndex(), time);

	for (int i = 0; i < _players.size(); i++)
	{

		vector<User*>::iterator s = _players.begin();
		++s;

		for (int j = 0; j < i; j++)
		{

			++s;
		}
		(*s)->send("120" + to_string(_questions[_currQuestionIndex]->getCorrectAnswerIndex() == answerNo));
	}

	return handleNextTurn();
}

bool Game::leaveGame(User* currUser)
{

	if (find(_players.begin(), _players.end(), currUser) != _players.end()) _players.erase(find(_players.begin(), _players.end(), currUser));

	return handleNextTurn();
}

int Game::getID()
{

	return 17;
}

bool Game::insertGameToDB()
{

	return true;
}

void Game::initQuestionsFromDB()
{

}

void Game::sendQuestionToAllUsers()
{

	_currentTurnAnswers = 0;

	string msg = "118";
	msg += to_string(_questions[_questions_no]->getQuestion().length());
	msg += _questions[_questions_no]->getQuestion();

	for (int i = 0; i < (sizeof(_questions[_questions_no]->getAnswers()) / sizeof(*_questions[_questions_no]->getAnswers())); i++)
	{

		msg += to_string(_questions[_questions_no]->getAnswers()[i].length());
		msg += _questions[_questions_no]->getAnswers()[i];
	}

	for (int i = 0; i < _players.size(); i++)
	{
		try
		{
			vector<User*>::iterator s = _players.begin();
			++s;

			for (int j = 0; j < i; j++)
			{

				++s;
			}
			(*s)->send(msg);
		}
		catch (...) {}
	}
}