#pragma once
#include <vector>
#include <string>
using namespace std;
//l
#include "Question.h"

class DataBase
{

public:
	DataBase();

	~DataBase();

	bool isUserExists(std::string username);

	bool addNewUser(std::string username, std::string password, std::string email);

	bool isUserAndPassMatch(std::string username, std::string password);

	std::vector<Question*> initQuestions(int questionNo);

	std::vector<std::string> getBestScores();

	std::vector<std::string> getPersonalStatus(std::string s);

	int insertNewGame();

	bool updateGameStatus(int a);

	bool addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime);

private:
	static int callbackCount(void* a, int b, char** c, char** d);

	static int callbackQuestions(void* a, int b, char** c, char** d);

	static int callbackBestScores(void* a, int b, char** c, char** d);

	static int callbackPersonalStatus(void* a, int b, char** c, char** d);
};
