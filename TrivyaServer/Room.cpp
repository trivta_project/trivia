#include "Room.h"

Room::Room(int id, User* admin, std::string name, int maxUsers, int questionNo, int questionTime)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionNo = questionNo;
	_questionTime = questionTime;
	_users.push_back(admin);
}

bool Room::joinRoom(User* user)
{

	if (_users.size() < _maxUsers)
	{

		_users.push_back(user);
		//TODO: send success message to user connecting

		for (int i = 0; i < _users.size(); i++)
		{

			_users[i]->send(getUsersListMessage());
		}
		return true;
	}

	//TODO: send failure message to user connecting
	return false;
}

void Room::leaveRoom(User* user)
{

	if (std::find(_users.begin(), _users.end(), user) != _users.end())
	{

		_users.erase(std::find(_users.begin(), _users.end(), user));

		for (int i = 0; i < _users.size(); i++)
		{

			(_users[i])->send(getUsersListMessage());
			//TODO: send user disconnecting message
		}
	}
}

int Room::closeRoom(User* user)
{

	if (user == _admin)
	{

		for (int i = 0; i < _users.size(); i++)
		{

			if (_users[i] != _admin)
			{


				_users[i]->clearRoom();
				//TODO: send closing message
			}
		}
		return _id;
	}

	return -1;
}

std::vector<User*> Room::getUser()
{

	return _users;
}

std::string Room::getUsersListMessage()
{

	return "";
}

int Room::getQuestionNo()
{

	return _questionNo;
}

int Room::getId()
{

	return _id;
}

std::string Room::getName()
{

	return _name;
}

std::string Room::getUsersAsString(std::vector<User*> usersList, User* excludeUser)
{

	std::string str = "";

	for (int i = 0; i < usersList.size(); i++)
	{

		if (usersList[i] != excludeUser) str += usersList[i]->getUsername() + ",";
	}

	return str.substr(0, str.length() - 1);
}

void Room::sendMessage(std::string message)
{

	sendMessage(NULL, message);
}

void Room::sendMessage(User* excludeUser, std::string message)
{

	for (int i = 0; i < _users.size(); i++)
	{

		if (_users[i] != excludeUser) _users[i]->send(message);
	}
}