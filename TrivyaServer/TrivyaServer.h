#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include "User.h"
#include <queue>
#include <mutex>
#include <iostream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <string>
#include <thread>
#include <vector>
#include "RecievedMessage.h"

#pragma comment (lib, "Ws2_32.lib")

using namespace std;

class TriviaServer
{

private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	//DataBase _db;
	map<std::string, std::string> users = { {"a","a"},{"b","b"} };
	map<int, Room*> _roomsList;
	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRecievedMessages;
	static int _roomIdSequence;

public:
	
	TriviaServer();
	~TriviaServer();

	void serve();

	void bindAndListen();
	
	void accept();

	void clientHandler(SOCKET client_socket);

	void safeDeleteUser(RecievedMessage* msg);

	User* handleSignIn(RecievedMessage* msg);

	bool handleSignUp(RecievedMessage* msg);

	void handleSignOut(RecievedMessage* msg);

	void handleLeaveGame(RecievedMessage* msg);

	void handleStartGame(RecievedMessage* msg);

	void handlePlayerAnswer(RecievedMessage* msg);

	bool handleCreateRoom(RecievedMessage* msg);
	
	bool handleCloseRoom(RecievedMessage* msg);
	
	bool handleJoinRoom(RecievedMessage* msg);
	
	bool handleLeaveRoom(RecievedMessage* msg);

	void handleGetUsersInRoom(RecievedMessage* msg);

	void handleGetRooms(RecievedMessage* msg);

	void handleGetBestScores(RecievedMessage* msg);

	void handleGetPersonalStatus(RecievedMessage* msg);

	void handleRecievedMessages();

	void addRecievedMessage(RecievedMessage* msg);

	RecievedMessage* buildRecieveMessage(SOCKET client_socket, int msgCode);
	
	User* getUserByName(std::string username);
	
	User* getUserBySocket(SOCKET client_socket);

	Room* getRoomById(int roomId);
};