#include "User.h"

User::User(std::string username, SOCKET sock)
{
	_username = username;
	_sock = sock;
	_currRoom = nullptr;
	_currGame = nullptr;
}

void User::send(std::string message)
{
	Helper::sendData(_sock, message);
}

std::string User::getUsername()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room * User::getRoom()
{
	return _currRoom;
}

Game * User::getGame()
{
	return _currGame;
}

void User::setGame(Game * gm)
{
	_currGame = gm;
	_currRoom = nullptr;
}

//void User::setRoom(Room * rm)
//{
//
//}

void User::clearRoom()
{
	setGame(nullptr);
}

bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionNo, int questionTime)
{
	if (getRoom() != nullptr)
	{
		send("1141");
		return false;
	}
	else
	{
		//_currRoom = new Room()
		send("1140");
		return true;
	}
}

bool User::joinRoom(Room * newRoom)
{
	if ( _currRoom != nullptr)
	{
		return false;
	}
	else
	{

	}
}

void User::leaveRoom()
{
}

int User::closeRoom()
{
	return 17;
}

bool User::leaveGame()
{
	return true;
}
