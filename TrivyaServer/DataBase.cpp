#include "DataBase.h"

DataBase::DataBase()
{
}

DataBase::~DataBase()
{
}

bool DataBase::isUserExists(std::string username)
{
	return true;
}

bool DataBase::addNewUser(std::string username, std::string password, std::string email)
{
	return true;
}

bool DataBase::isUserAndPassMatch(std::string username, std::string password)
{
	return true;
}

std::vector<Question*> DataBase::initQuestions(int questionNo)
{
	return std::vector<Question*>();
}

std::vector<std::string> DataBase::getBestScores()
{
	return std::vector<std::string>();
}

std::vector<std::string> DataBase::getPersonalStatus(std::string s)
{
	return std::vector<std::string>();
}

int DataBase::insertNewGame()
{
	return 17;
}

bool DataBase::updateGameStatus(int a)
{
	return true;
}

bool DataBase::addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime)
{
	return true;
}

int DataBase::callbackCount(void * a, int b, char ** c, char ** d)
{
	return 0;
}

int DataBase::callbackQuestions(void * a, int b, char ** c, char ** d)
{
	return 0;
}

int DataBase::callbackBestScores(void * a, int b, char ** c, char ** d)
{
	return 0;
}

int DataBase::callbackPersonalStatus(void * a, int b, char ** c, char ** d)
{
	return 0;
}
