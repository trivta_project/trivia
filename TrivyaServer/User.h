#pragma once
#include "Room.h"
#include "Game.h"
#include "Helper.h"
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>

class Game;
class Room;

class User
{

private:
	std::string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
	
public:
	User(std::string username, SOCKET sock);
	

	void send(std::string message);

	std::string getUsername();


	SOCKET getSocket();

	Room* getRoom();

	Game* getGame();

	void setGame(Game* gm);
	//void setRoom(Room* rm);

	void clearRoom();

	bool createRoom(int roomId, std::string roomName, int maxUsers, int questionNo, int questionTime);

	bool joinRoom(Room* newRoom);

	void leaveRoom();

	int closeRoom();

	bool leaveGame();
};
