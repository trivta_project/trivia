#pragma once
#include <vector>
#include "User.h"
#include <iostream>

class User;

class Room
{

private:
	std::vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	std::string _name;
	int _id;

public:
	Room(int id, User* admin, std::string name, int maxUsers, int questionNo, int questionTime);

	bool joinRoom(User* user);

	void leaveRoom(User* user);

	int closeRoom(User* user);

	std::vector<User*> getUser();

	std::string getUsersListMessage();

	int getQuestionNo();

	int getId();

	std::string getName();

	std::string getUsersAsString(std::vector<User*> usersList, User* excludeUser);

	void sendMessage(std::string message);

	void sendMessage(User* excludeUser, std::string message);
};