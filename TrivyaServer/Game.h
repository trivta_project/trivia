#pragma once

#include "Question.h"
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include "DataBase.h"
#include "User.h"

using namespace std;

class User;

class Game
{

private:
	std::vector<Question*> _questions;
	std::vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db = DataBase();
	map<std::string, int> _results;
	int _currentTurnAnswers;

public:
	Game(const std::vector<User*>& players, int questionNo, DataBase& db);
	
	~Game();
	
	void sendFirstQuestion();

	void handleFinishGame();
	
	bool handleNextTurn();
	
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	
	bool leaveGame(User* currUser);
	
	int getID();
	
	bool insertGameToDB();
	
	void initQuestionsFromDB();
	
	void sendQuestionToAllUsers();
};