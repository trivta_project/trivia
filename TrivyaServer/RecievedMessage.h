#pragma once

#include "Room.h"
#include "Game.h"
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>
#include "User.h"
#include <vector>

class RecievedMessage
{

private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	std::vector<string> _values;

public:
	RecievedMessage(SOCKET sock, int messageCode)
	{

		_sock = sock;
		_messageCode = messageCode;
	}
	RecievedMessage(SOCKET sock, int messageCode, std::vector<string> values)
	{

		_sock = sock;
		_messageCode = messageCode;
		for (int i = 0; i < values.size(); i++)
		{

			_values.push_back(values[i]);
		}
	}

	SOCKET getSock()
	{

		return _sock;
	}

	User* getUser()
	{

		return _user;
	}

	void setUser(User* user)
	{

		_user = user;
	}

	int getMessageCode()
	{

		return _messageCode;
	}

	std::vector<string> getValues()
	{

		return _values;
	}
};